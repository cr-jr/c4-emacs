;;; early-init.el -*- lexical-binding: t; -*-
;;;
;;; Summary: A good chunk of my early init configuration draws heavy
;;; inspiration from @mfiano's emacs config. I think it's a great idea
;;; in particular to isolate packages to the cache and keep our Emacs
;;; config directory clean.
;;;
;;; Of course, I make my own tweaks as well.

;;; ============================================================================
;;; Emacs startup optimizations
;;; ============================================================================

;; Garbage collection slows down startup time, so we maximize the threshold for
;; it to run, and we will later reset it.
(setq gc-cons-threshold most-positive-fixnum)

;; file-name-handler-alist is consulted on various I/O functions such as
;; REQUIRE, slowing down startup time, so we set it to NIL, and establish a hook
;; to restore when Emacs is finished starting.
(unless (or (daemonp) noninteractive)
  (let ((file-name-handler-alist/old file-name-handler-alist))
    (setq file-name-handler-alist nil)
    (add-hook 'emacs-startup-hook
              (lambda ()
                (let ((value (delete-dups
                              (append file-name-handler-alist
                                      file-name-handler-alist/old))))
                  (setq file-name-handler-alist value))))))

(unless (daemonp)
  (advice-add #'tty-run-terminal-initialization :override #'ignore)
  (add-hook 'window-setup-hook
            (lambda ()
              (advice-remove #'tty-run-terminal-initialization #'ignore)
              (tty-run-terminal-initialization (selected-frame) nil t))))

;; Set frame parameters early for faster startup.
(setq frame-inhibit-implied-resize t)
(setq frame-resize-pixelwise t)
(setq truncate-partial-width-windows nil)
(add-to-list 'default-frame-alist '(font . "Input Mono 11"))

;; Set some useful defaults
(blink-cursor-mode 0)
(setq-default cursor-type 'bar)

(setq frame-title-format (concat "%b - GNU Emacs at " system-name))
(setq column-number-mode t)
(setq display-line-numbers t)

(menu-bar-mode 0)
(scroll-bar-mode 0)
(set-fringe-mode 8)
(tool-bar-mode 0)
(tooltip-mode 0)

(setq inhibit-startup-message t)
(setq initial-scratch-message "")
(setq initial-major-mode 'text-mode)
(setq visible-bell t)
(global-auto-revert-mode t)
(delete-selection-mode 1)
(fset 'yes-or-no-p 'y-or-n-p)

;;; ============================================================================
;;; Specify some directory paths
;;; ============================================================================

;; For the rest of the Emacs configuration, set this directory to something
;; inside the standard cache directory, so we do not pollute our emacs.d
;; directory with files that we would then have to ignore with Git.
(defvar c4/config-dir user-emacs-directory)
(setq user-emacs-directory
      (expand-file-name "emacs/" (or (getenv "XDG_CACHE_HOME") "~/.cache/")))

;; Add our custom lisp modules to the Emacs load path so they can be discovered.
(add-to-list 'load-path (expand-file-name "modules" c4/config-dir))

;; For the list of native compilation ELN cache directories, delete all but the
;; last element, which is always assumed to be the system path, and then cons a
;; new path in our cache directory to the front. This effectively removes the
;; entry for the original ~/.emacs.d/eln-cache/ and any others that are
;; unwanted.
(defvar native-compile-target-directory
  (expand-file-name "eln-cache/" user-emacs-directory))

;;; ============================================================================
;;; Set up the package manager
;;; ============================================================================

;; Pre-configure the package manager settings before it is loaded.
(setq package-enable-at-startup nil)
(setq package-quickstart nil)
(setq straight-check-for-modifications '(check-on-save find-when-checking))
(setq straight-profiles `((nil . ,(expand-file-name "lockfile" c4/config-dir))))

;; Bootstrap straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; setup.el should also be initialized at this point
(straight-use-package 'setup)
(require 'setup)

;; Now I'll set up a straight.el macro for easy installation of packages
(setup-define :straight
  (lambda (recipe)
    `(unless (straight-use-package ',recipe)
       ,(setup-quit)))
  :documentation
  "Install RECIPE with `straight-use-package'.
This macro can be used as HEAD, and will replace itself with the
first RECIPE's package."
  :repeatable t
  :shorthand (lambda (sexp)
               (let ((recipe (cadr sexp)))
                 (if (consp recipe)
                     (car recipe)
                   recipe))))

(setup-define :face
    (lambda (face spec) `(custom-set-faces (quote (,face ,spec))))
    :documentation "Customize FACE to SPEC."
    :signature '(face spec ...)
    :debug '(setup)
    :repeatable t
    :after-loaded t)

(provide 'early-init)

;;; early-init.el ends here
