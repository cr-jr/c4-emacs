;;; C4 (CRJr's Coding Command Center) ---  -*- lexical-binding: t; -*-
;;;
;;; Summary: This is a revised version of my personal Emacs configuration
;;; that abandons the verbose Org mode configuration in favor of a compact,
;;; layered setup.

;; Drop garbage collection threshold post startup
(add-hook 'after-init-hook
	#'(lambda ()
	    (setq gc-cons-threshold
		    (/ most-positive-fixnum (expt 1024 3)))))

(setup c4:base (:require +clean +theme +keybindings +debug +utilities))

(setup c4:enhancements (:require +minibuffer +search +shell +project))

(setup c4:text (:require +text +org))

(setup c4:prog (:require +coding +lsp))

(setup c4:lisp (:require +lang-elisp +lang-lisp +lang-racket +lang-guile))

(setup c4:web (:require +env-web +lang-javascript))

(setup c4:system (:require +lang-rust))

(setup c4:experimental (:require +lang-raku))

(setup c4:os (:require +os-guix))

(provide 'init)
;;; init.el ends here
