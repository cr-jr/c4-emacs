;;; +clean.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains some helpful packages for keeping my Emacs
;;; clean and tidy.

(setq create-lockfiles nil)
(setq custom-file null-device)

(setup (:straight no-littering)
  (:require no-littering)
  (:option
    auto-save-file-name-transforms
    `((".*" ,(no-littering-expand-var-file-name (concat user-emacs-directory "auto-save/")) t))))

(setup autoparent
  (add-to-list 'find-file-not-found-functions #'c4/create-parent)

  (defun c4/create-parent ()
    "Ensures that the parent dirs are created for a nonexistent file."
    (let ((parent-directory (file-name-directory buffer-file-name)))
      (when (and (not (file-exists-p parent-directory))
		 (y-or-n-p (format
                            "Directory `%s' does not exist! Create it?"
                            parent-directory)))
	(make-directory parent-directory t)))))

(setup (:straight whitespace-cleanup-mode)
  (global-whitespace-cleanup-mode t))

(provide '+clean)
;;; +clean.el ends here
