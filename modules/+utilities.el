;;; +utilities.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains configuration for packages that enhance my
;;; core editing experience.

(setup (:straight helpful crux editorconfig)
  (:require helpful crux editorconfig)
  (:global
   "M-SPC k k" crux-create-scratch-buffer ; g45
   "M-SPC k l" crux-rename-file-and-buffer ; g46
   "M-SPC k m" crux-delete-file-and-buffer ; g47

   "M-SPC r y" helpful-at-point ; m02
   "M-SPC r f" helpful-function ; m03
   "M-SPC r g" helpful-command ; m04
   "M-SPC r h" helpful-callable ; m05
   "M-SPC r v" helpful-variable ; m06
   "M-SPC r b" helpful-key ; m07
   "M-SPC r n" helpful-symbol) ; m08
  (editorconfig-mode 1))

(provide '+utilities)
;;; +utilities.el ends here
