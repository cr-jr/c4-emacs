;;; +search.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains packages that enhance buffer and file searching.

(setup (:straight ctrlf)
  (:require ctrlf)
  (:global
    "M-r" ctrlf-backward-literal
    "M-R" ctrlf-backward-fuzzy
    "M-y" ctrlf-forward-literal
    "M-Y" ctrlf-forward-fuzzy)
  (:with-mode text-mode (:hook ctrlf-mode))
  (:with-mode prog-mode (:hook ctrlf-mode)))

(provide '+search)
;;; +search.el ends here
