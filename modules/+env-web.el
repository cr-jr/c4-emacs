;;; +env-web.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module sets up all the packages needed for my web development
;;; workflow. I make heavy use of skewer-mode when prototyping and debugging.

(setup (:straight skewer-mode impatient-mode emmet-mode rainbow-mode)
  (:require html-mode css-mode skewer-mode impatient-mode emmet-mode)
  (:with-mode (html-mode css-mode js-mode)
    (:hook rainbow-mode))
  (:with-mode (html-mode css-mode)
    (:hook emmet-mode))
  (:with-mode html-mode
    (:hook skewer-html-mode)
    (:hook impatient-mode))
  (:with-mode css-mode
    (:hook skewer-css-mode))
  (:with-mode js-mode
    (:hook skewer-mode))
  (:with-mode emmet-mode
    (:bind
      "M-SPC q q" emmet-preview ; M00
      "M-SPC q w" emmet-preview-accept ; M01
      "M-SPC q e" emmet-expand-line)) ; M02
  (:with-mode impatient-mode
    (:bind
      "M-SPC w q" httpd-start-hook ; M10
      "M-SPC w w" httpd-stop ; M11
      "M-SPC w e" httpd-serve-directory)) ; M12
  (:with-mode skewer-mode
    (:bind
      "M-SPC i u" skewer-eval-last-expression ; g10
      "M-SPC i i" skewer-eval-defun ; g11

      "M-SPC w q" skewer-load-buffer ; M10
      "M-SPC w w" run-skewer ; M11
      "M-SPC w e" skewer-run-phantomjs ; M12
      "M-SPC w a" skewer-eval-last ; M13
      "M-SPC w s" skewer-repl))) ; M14

(provide '+env-web)
;;; +env-web.el ends here
