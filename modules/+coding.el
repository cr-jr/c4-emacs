;;; +coding.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains global configuration and packages for my
;;; coding workflow. Specifically, it sets up syntax highlighting, formatting,
;;; and global linting and autocompletion behavior.

(setup (:straight tree-sitter tree-sitter-langs)
  (set-face-attribute 'font-lock-comment-face nil
    :slant 'italic
    :weight 'light
    :foreground (ewal-load-color 'red))
  (set-face-attribute 'font-lock-keyword-face nil
    :foreground (ewal-load-color 'blue))
  (set-face-attribute 'font-lock-function-name-face nil
    :weight 'bold
    :foreground (ewal-load-color 'yellow))
  (set-face-attribute 'font-lock-string-face nil
    :slant 'italic
    :foreground (ewal-load-color 'green))
  (set-face-attribute 'font-lock-doc-face nil
    :weight 'bold)
  (set-face-attribute 'font-lock-constant-face nil
    :inherit 'font-lock-function-name-face)
  (set-face-attribute 'font-lock-builtin-face nil
    :inherit 'font-lock-keyword-face)
  (set-face-attribute 'font-lock-variable-name-face nil
    :inherit 'font-lock-function-name-face))

(setup (:straight
         rainbow-delimiters
         color-identifiers-mode
         smartparens
         aggressive-indent
         flycheck
         apheleia)
  (:with-mode prog-mode
    (:require smartparens-config)
    (:hook smartparens-mode)
    (:hook aggressive-indent-mode)
    (:hook rainbow-delimiters-mode)
    (:hook prettify-symbols-mode)
    (:hook color-identifiers-mode)
    (:hook flycheck-mode)
    (:hook apheleia-mode)))

(setup (:straight corfu corfu-doc kind-icon)
  (:require corfu kind-icon)
  (:option corfu-auto t
    corfu-quit-no-match t
    completion-cycle-threshold 3
    tab-always-indent 'complete
    kind-icon-default-face 'corfu-default
    kind-icon-use-icons nil)
  (:with-mode corfu-mode
    (:hook corfu-doc-mode)
    (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))
  (global-corfu-mode))

(provide '+coding)
;;; +coding.el ends here
