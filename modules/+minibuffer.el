;;; +minibuffer.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains packages that enhance the functionality of the
;;; minibuffer. This includes command lookup, buffer switching, and history.

(setup (:straight vertico orderless marginalia)
  (:require vertico orderless marginalia)
  (vertico-mode)
  (savehist-mode)
  (:option completion-styles '(orderless))
  (:option marginalia-annotators
    '(marginalia-annotators-heavy marginalia-annotators-light))
  (marginalia-mode 1))

(setup (:straight consult)
  (:require consult)
  (:global
    "M-g" consult-line
    "M-G" consult-ripgrep

    "M-SPC o u" consult-buffer ; g20
    "M-SPC o i" consult-buffer-other-window ; g21

    "M-SPC /" consult-complex-command)
  (:option register-preview-delay 0
    register-preview-function #'consult-register-window
    consult-narrow-key "<"))

(provide '+minibuffer)
;;; +minibuffer.el ends here
