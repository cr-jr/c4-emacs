;;; +lang-lisp.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains all of my configuration and settings for
;;; working with Common Lisp.

(setup (:straight sly)
  (:require sly)
  (:with-mode lisp-mode
    (:option inferior-lisp-program "sbcl")
    (:hook sly-mode)
    (:bind
      "M-SPC q q" sly ; M00
      "M-SPC q w" sly-list-connections ; M01
      "M-SPC q e" sly-next-connection ; M02
      "M-SPC q a" sly-prev-connection ; M03
      "M-SPC q s" sly-load-file ; M04
      "M-SPC q d" next-error ; M05

      "M-SPC w q" sly-next-note ; M10
      "M-SPC w w" sly-previous-note ; M11
      "M-SPC w e" sly-remove-notes ; M12

      "M-SPC e q" sly-documentation ; M20
      "M-SPC e w" sly-info ; M21

      "M-SPC a q" sly-compile-defun ; M30
      "M-SPC a w" sly-compile-region ; M31
      "M-SPC a e" sly-compile-file ; M32
      "M-SPC a a" sly-compile-and-load-file ; M33

      "M-SPC s q" sly-eval-last-expression ; M40
      "M-SPC s w" sly-pprint-eval-last-expression ; M41
      "M-SPC s e" sly-interactive-eval ; M42
      "M-SPC s a" sly-eval-defun ; M43
      "M-SPC s s" sly-eval-region ; M44
      "M-SPC s d" sly-pprint-eval-region ; M45

      "M-SPC d q" sly-expand-1 ; M50
      "M-SPC d w" sly-macroexpand-all ; M51
      "M-SPC d e" sly-compiler-macroexpand-1 ; M52
      "M-SPC d s" sly-format-string-expand ; M53
      "M-SPC d d" sly-macroexpand-again ; M54
      "M-SPC d z" sly-macroexpand-1-inplace ; M55
      "M-SPC d x" sly-macroexpand-again ; M56
      "M-SPC d c" sly-macroexpand-undo ; M57

      "M-SPC z q" sly-describe-symbol ; M60
      "M-SPC z w" sly-describe-function ; M61
      "M-SPC z e" sly-apropos ; M62
      "M-SPC z a" sly-apropos-all ; M63
      "M-SPC z s" sly-apropos-package ; M64
      "M-SPC z d" sly-hyperspec-lookup ; M65

      "M-SPC x q" sly-edit-uses ; M70
      "M-SPC x w" sly-who-calls ; M71
      "M-SPC x e" sly-calls-who ; M72
      "M-SPC x a" sly-who-references ; M73
      "M-SPC x s" sly-who-binds ; M74
      "M-SPC x d" sly-who-sets ; M75
      "M-SPC x z" sly-who-macroexpands ; M76
      "M-SPC x x" sly-who-specializes))) ; M77

(provide '+lang-lisp)
;;; +lang-lisp.el ends here
