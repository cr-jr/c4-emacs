;;; +debug.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains useful packages to help me debug my setup.
;;; This module is helpful especially for debugging the heavier packages
;;; throwing their weight around in my configuration.

(setup (:straight esup bug-hunter)
  (:require esup bug-hunter)
  (:global
   "M-SPC u j" esup ; g03
   "M-SPC u k" bug-hunter-init-file)) ; g04

(provide '+debug)
;;; +debug.el ends here
