;;; +lang-raku.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains my setup for working with Raku. It mainly
;;; relies on raku-mode configuration.

(setup (:straight raku-mode '(ob-raku :host github :repo "cr-jr/ob-raku"))
  (:require raku-mode)
  (:with-mode raku-mode
    (:bind
      "M-SPC q q" raku-send-line-to-repl ; M00
      "M-SPC q w" raku-send-region-to-repl ; M01
      "M-SPC q e" raku-send-buffer-to-repl ; M02

      "M-SPC w w" raku-mode-menu) ; M10
    (:file-match "\\.rakumod\\'" "\\.raku\\'"))
  (:with-mode org-mode
    (add-to-list 'org-babel-load-languages '(raku . t))
    (add-to-list 'org-src-lang-modes '("raku" . raku))))

(provide '+lang-raku)
;;; +lang-raku.el ends here
