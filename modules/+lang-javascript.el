;;; +lang-javascript.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains my setup and configuration for working with
;;; JavaScript. I primarily focus on the Deno runtime because it's the closest
;;; to a web native engine without actually being in a browser.
;;;
;;; This simplifies the amount of tooling I need for my projects (often none).

(setup (:straight (ob-deno :host github :repo "cr-jr/ob-deno"))
  (:require eglot)
  (:with-mode js-mode
    (:hook eglot-ensure))
  (:with-mode org-mode
    (add-to-list 'org-babel-load-languages '(deno . t))
    (add-to-list 'org-src-lang-modes '("deno" . js)))
  (defclass eglot-deno (eglot-lsp-server) ()
    :documentation "A custom class deno lsp")
  (cl-defmethod eglot-initialization-options ((server eglot-deno))
    "Passes through required deno initialization options."
    (list :enable t
      :lint t))
  (add-to-list
    'eglot-server-programs '(((js-mode :language-id "javascript")) . (eglot-deno "deno" "lsp")))
  (add-hook
    'js-mode-hook (lambda () (add-hook 'before-save-hook 'eglot-format-buffer))))

(provide '+lang-javascript)
;;; +lang-javascript.el ends here
