;;; +lsp.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains the base LSP setup for the programming
;;; languages I use regularly and study.

(setup (:straight eglot)
  (:require eglot)
  (:with-hook eglot--managed-mode
    (:bind
      "M-SPC q q" eglot ; M00
      "M-SPC q w" eglot-reconnect ; M01
      "M-SPC q e" eglot-shutdown ; M02
      "M-SPC q a" eglot-events-buffer ; M03
      "M-SPC q s" eglot-stderr-buffer ; M04
      "M-SPC q d" eglot-code-actions ; M05
      "M-SPC q z" eglot-rename ; M06
      "M-SPC q x" eglot-format ; M07
      "M-SPC q c" eldoc))) ; M08

(provide '+lsp)
;;; +lsp.el ends here
