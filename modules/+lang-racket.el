;;; +lang-racket.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains all the setup and configuration for
;;; programming with Racket source code and with Org-babel.

(setup (:straight racket-mode (ob-racket :host github :repo "DEADB17/ob-racket"))
  (:require racket-mode)
  (:with-mode racket-mode
    (:option racket-program "racket")
    (:hook racket-xp-mode)
    (:hook racket-smart-open-bracket-mode)
    (:hook racket-unicode-input-method-enable)
    (:bind
      "M-SPC q q" racket-run ; M00
      "M-SPC q w" racket-run-and-switch-repl ; M01
      "M-SPC q e" racket-run-module-at-point ; M02

      "M-SPC w q" racket-send-last-sexp ; M10
      "M-SPC w w" racket-send-definition ; M11
      "M-SPC w e" racket-send-region ; M12

      "M-SPC e q" racket-test ; M20
      "M-SPC e w" racket-fold-all-tests ; M21
      "M-SPC e e" racket-unfold-all-test)) ; M22
  (:with-mode org-mode
    (add-to-list 'org-babel-load-languages '(racket . t))
    (add-to-list 'org-babel-load-languages '(scribble . t))))

(provide '+lang-racket)
;;; +lang-racket.el ends here
