;;; +org.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains my configuration specifically for Org-mode
;;; and my chosen addons. This includes making Org files more pleasant to edit,
;;; adding some visual enhancements, and making it incredibly easy to publish
;;; and export documents from Org-mode.

(setup (:straight (org :type built-in))
  (:require org)
  (defhydra c4/org-browse (:timeout 30)
    "A transient mode to logically browse an Org file"
    ("h" org-forward-heading-same-level "jump to next heading (same level)")
    ("H" org-backward-heading-same-level "jump to prev heading (same level)")
    ("s" org-babel-next-src-block "jump to next src block")
    ("S" org-babel-previous-src-block "jump to prev src block")
    ("v" org-next-visible-heading "jump to next heading")
    ("V" org-previous-visible-heading "jump to prev heading")
    ("RET" nil "exit state: org-browse" :exit t))

  (defhydra c4/org-filing (:timeout 30)
    "A transient mode to rearrange things"
    ("i" org-move-item-up "move item up")
    ("I" org-move-subtree-up "move subtree up")
    ("k" org-move-item-down "move item down")
    ("K" org-move-subtree-down "move subtree down")
    ("RET" nil "exit state: org-filing" :exit t))

  (defun my/org-refile-in-file (&optional prefix)
    "Refile to a target within the current file."
    (interactive)
    (let ((org-refile-targets `(((,(buffer-file-name)) :maxlevel . 6))))
      (call-interactively 'org-refile)))

  (:with-mode org-mode
    (:bind
      "M-N" org-mark-element
      "M-M" org-babel-mark-block

      "M-SPC j k" org-toggle-narrow-to-subtree ; g34
      "M-SPC j l" org-narrow-to-block ; g35
      "M-SPC j m" org-narrow-to-element ; g36

      "M-SPC q q" org-src-execute-src-block ; M00

      "M-SPC w q" org-edit-special ; M10
      "M-SPC w w" org-babel-tangle ; M11

      "M-SPC e q" org-insert-link ; M20
      "M-SPC e w" c4/org-filing/body ; M21
      "M-SPC e e" my/org-refile-in-file ; M22
      "M-SPC e a" c4/org-browse/body)) ; M23

  (:with-mode org-src-mode
    (:bind
      "M-SPC q q" org-edit-src-exit ; M00
      "M-SPC q w" org-edit-src-abort))) ; M01

(setup org:ui
  (:with-mode org-mode
    (defvar c4/org-measure
      (c4/round-height (/ c4/font-size-doc c4/font-ratio)))
    (:option org-element-use-cache nil
      org-ellipsis " ➕"
      org-directory "~/Documents/Org/"
      fill-column c4/org-measure
      line-spacing 0.25)
    (:face org-code ((t (:inherit 'fixed-pitch))))
    (:face org-tag ((t (:inherit 'org-code))))
    (:face org-table ((t (:inherit 'org-code))))
    (:face org-verbatim ((t (:inherit 'org-code))))
    (:face org-ellipsis ((t (:underline nil))))
    (:face org-meta-line ((t (:inherit 'org-code :extend t))))
    (:face org-block ((t (:inherit 'fixed-pitch))))
    (:face org-block-begin-line ((t (:inherit 'fixed-pitch))))
    (:face org-block-end-line ((t (:inherit 'org-block-begin-line))))
    (:hook variable-pitch-mode)
    (:hook org-indent-mode)
    (:hook visual-line-mode)
    (:hook auto-fill-mode)))

(setup org:refiling
  (:with-mode org-mode
    (:option org-refile-use-outline-path 'file
      org-outline-path-complete-in-steps nil
      org-refile-allow-creating-parent-nodes 'confirm)
    (advice-add 'org-refile :after 'org-save-all-org-buffers)))

(setup org:babel
  (:with-mode org-mode
    (:option org-src-fontify-natively t
      org-confirm-babel-evaluate nil
      org-src-tab-acts-natively t
      org-src-preserve-indentation t
      org-babel-lisp-eval-fn #'sly-eval)
    (org-babel-do-load-languages 'org-babel-load-languages
      '((emacs-lisp . t)
         (lisp . t)
         (scheme . t)
         (C . t)
         (shell . t)
         (js . t)))))

(setup (:straight toc-org)
  (:require toc-org)
  (:with-mode org-mode
    (:hook toc-org-mode)))

(setup (:straight weblorg)
  (:require weblorg))

(setup (:straight ox-gfm)
  (:require ox-gfm))

(provide '+org)
;;; +org.el ends here
