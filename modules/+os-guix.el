;;; +os-guix.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains my packages for working with GNU Guix; my
;;; operating system of choice.

(setup (:straight guix)
  (:require guix))

(provide '+os-guix)
;;; +os-guix.el ends here
