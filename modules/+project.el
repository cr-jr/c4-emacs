;;; +project.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains packages that augment how I manage my projects.
;;; My project workflow relies heavily on Magit in particular.

(setq user-full-name "Chatman R. Jr")
(setq user-mail-address "crjr.code@protonmail.com")

(setup (:straight magit diff-hl)
  (:require magit diff-hl)
  (:global
    "M-SPC , u" magit ; g70
    "M-SPC , i" magit-diff ; g71
    "M-SPC , o" magit-init ; g72
    "M-SPC , j" magit-push ; g73
    "M-SPC , k" magit-pull ; g74
    "M-SPC , l" magit-remote ; g75
    "M-SPC , m" magit-stage ; g76
    "M-SPC , ," magit-unstage) ;g77
  (:with-mode magit-pre-refresh (:hook diff-hl-magit-pre-refresh))
  (:with-mode magit-post-refresh (:hook diff-hl-magit-post-refresh))
  (global-diff-hl-mode 1))

(provide '+project)
;;; +project.el ends here
