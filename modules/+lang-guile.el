;;; +lang-guile.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains my setup and configuration for programming
;;; with GNU Guile. Geiser does most of the work here.

(setup (:straight geiser-guile macrostep-geiser)
  (:require geiser geiser-guile)
  (:with-mode scheme-mode
    (:option geiser-guile-binary "guile"
      geiser-active-implementations '(guile)
      geiser-default-implementation 'guile)
    (:hook geiser-mode)))

(provide '+lang-guile)
;;; +lang-guile.el ends here
