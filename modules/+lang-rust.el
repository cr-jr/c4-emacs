;;; +lang-rust.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains my packages for working with Rust. It's
;;; built mainly around a very simple rustic configuration.

(setup (:straight rustic)
  (:option
    rustic-analyzer-command '("~/.nix-profile/bin/rust-analyzer")
    rustic-lsp-client 'eglot)
  (:with-mode rustic-mode
    (:file-match "\\.rs\\'")
    (:hook eglot-ensure)))

(provide '+lang-rust)
;;; +lang-rust.el ends here
