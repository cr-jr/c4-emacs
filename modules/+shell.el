;;; +shell.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains enhancements for using the shell/terminal
;;; as part of my workflow.

(setup (:straight eshell-prompt-extras shell-pop)
  (:require eshell-prompt-extras shell-pop)
  (:global
    "M-SPC '" shell-pop
    "M-SPC \"" eshell)
  (:option eshell-highlight-prompt nil
    eshell-prompt-function 'epe-theme-lambda)
  (:option shell-pop-window-size 30
    shell-pop-shell-type (quote ("eshell" "*Eshell*" (lambda nil (eshell))))))

(provide '+shell)
;;; +shell.el ends here
