;;; +text.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains some handy tools for text modes. It focuses
;;; on a general setup that benefits all text modes.
;;;
;;; Mode-specific modules are defined later.

(setup (:straight languagetool mw-thesaurus)
  (:require languagetool mw-thesaurus)
  (:option languagetool-java-arguments '("-Dfile.encoding=UTF-8")
    languagetool-console-command "~/.nix-profile/share/languagetool-commandline.jar"
    languagetool-server-command "~/.nix-profile/share/languagetool-server.jar"
    languagetool-default-language "en-US")
  (:with-mode text-mode
    (:bind
      "M-SPC t r" languagetool-check
      "M-SPC t t" languagetool-clear-suggestions
      "M-SPC t y" languagetool-correct-at-point
      "M-SPC t f" languagetool-correct-buffer
      "M-SPC t g" mw-thesaurus-lookup-dwim))
  (:option mw-thesaurus--api-key "629ccc6a-d13c-47dc-a3bd-4f807b3b90a6"))

(provide '+text)
;;; +text.el ends here
