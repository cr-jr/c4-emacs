;;; +theme.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: this file sets up cosmetic UI enhancements that don't have much
;;; if any impact on functionality. Skinning is the easiest way to denote that
;;; something has changed from the vanilla setup, so I'm beginning with the
;;; fruit on the ground.

;;; Linum behavior

(setup linum
  (add-hook 'prog-mode-hook
          (if (and (fboundp 'display-line-numbers-mode) (display-graphic-p))
              #'display-line-numbers-mode
            #'linum-mode))
  (dolist (mode '(
                org-mode-hook
                term-mode-hook
                shell-mode-hook
                treemacs-mode-hook
                vterm-mode
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0)))))

;; Give buffers unique names
(setup buffer
  (:option uniquify-buffer-name-style 'post-forward-angle-brackets))

;;; Theme setup

(setup (:straight ewal ewal-doom-themes mood-line)
  (:require ewal mood-line)
  (:option
   ewal-use-built-in-always-p nil
   ewal-use-built-in-on-failure-p t
   ewal-built-in-palette "sexy-material"
   ewal-json-file "~/.cache/wal/colors.json")
  (ewal-load-colors)
  (load-theme 'ewal-doom-vibrant t)
  (mood-line-mode))

;;; Typography

(setup typography
  (defun c4/round-height (height)
    "Rounds the input HEIGHT to the nearest ten."
    (let* ((~height~ (truncate height))
	   (*height* (round ~height~ 10))) (* *height* 10)))

  ;; Font settings
  (defvar c4/font "Input Sans")
  (defvar c4/font-mono "Input Mono")
  (defvar c4/font-size 120)
  (defvar c4/font-ratio 1.25)

  (defvar c4/font-size-doc
    (c4/round-height (* c4/font-size c4/font-ratio)))

  ;; Default font
  (set-face-attribute 'default nil
		      :family c4/font :height c4/font-size :foreground (ewal-load-color 'white))

  ;; Code font
  (set-face-attribute 'fixed-pitch nil
		      :family c4/font :width 'condensed :weight 'regular :height c4/font-size :foreground (ewal-load-color 'white))

  ;; Enlarge for Org-mode
  (set-face-attribute 'variable-pitch nil :family c4/font :height c4/font-size-doc)

  ;; Set a line number style
  (set-face-attribute 'line-number nil  :family c4/font-mono :height c4/font-size)
  (set-face-attribute 'line-number-current-line nil
		      :inherit 'line-number :weight 'semi-bold :foreground (ewal-load-color 'white)))

(provide '+theme)
