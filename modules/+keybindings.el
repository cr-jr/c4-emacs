;;; +keybindings.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains my keybinding setup. The reason I place it
;;; before all other modules in this configuration is because if I can't
;;; effectively navigate Emacs in an ergonomic way, then I can't really take
;;; advantage of any other customizations.
;;;
;;; I use a custom layout that maps common editing actions to Meta and
;;; assigns a command keymap to an M-SPC prefix.
;;;
;;; - Meta because my thumbs are always on or near Alt.
;;; - Meta-SPC because my thumbs are always near both.
;;; - Menu for M-x because my thumb can comfortably curl to press it for the
;;; few times I need to run an explicit command
;;;
;;; Unlike previous iterations, I opted out of Vim-style modality and instead
;;; prioritize making the editing experience more pleasant for the physiology of
;;; my hands.

(setup (:straight which-key meow hydra)
  (:require which-key meow hydra)
  (:option
    meow-visit-sanitize-completion nil
    meow-keypad-start-keys nil
    meow-keypad-meta-prefix nil
    meow-keypad-ctrl-meta-prefix nil
    meow-self-insert-undefined nil)
  (which-key-mode))

(setup actions:exec
  (unless (display-graphic-p)
    (:global
      "M-`" execute-extended-command)))

(setup actions:modifiers
  (:global
   "M-0" meow-digit-argument
   "M-1" meow-digit-argument
   "M-2" meow-digit-argument
   "M-3" meow-digit-argument
   "M-4" meow-digit-argument
   "M-5" meow-digit-argument
   "M-6" meow-digit-argument
   "M-7" meow-digit-argument
   "M-8" meow-digit-argument
   "M-9" meow-digit-argument
   "M-p" negative-argument
   "M-;" meow-reverse
   "M-/" repeat))

(setup actions:insertion
  (:global "M-<return>" meow-open-above))

(setup actions:movement
  (:global
   "M-i" meow-prev
   "M-I" scroll-down-command
   "M-j" meow-left
   "M-J" backward-word
   "M-k" meow-next
   "M-K" scroll-up-command
   "M-l" meow-right
   "M-L" forward-to-word
   "M-u" beginning-of-line
   "M-U" beginning-of-buffer
   "M-o" end-of-line
   "M-O" end-of-buffer))

(setup actions:selection
  (:global
   "M-w" meow-prev-expand
   "M-a" meow-left-expand
   "M-A" meow-back-word
   "M-s" meow-next-expand
   "M-d" meow-right-expand
   "M-D" meow-next-word
   "M-q" meow-pop-selection
   "M-Q" meow-cancel-selection
   "M-e" meow-grab
   "M-E" meow-pop-grab
   "M-n" meow-mark-word
   "M-N" meow-mark-symbol
   "M-m" meow-line
   "M-M" meow-block
   "M-," meow-bounds-of-thing
   "M-<" meow-beginning-of-thing
   "M-." meow-inner-of-thing
   "M->" meow-end-of-thing))

(setup actions:search
  (:global
   "M-t" meow-till
   "M-T" meow-find
   "M-f" meow-search
   "M-F" meow-visit))

(setup actions:edit
  (:global
   "M-z" meow-undo
   "M-Z" undo-redo
   "M-x" meow-kill
   "M-X" meow-kill-whole-line
   "M-c" meow-save
   "M-C" meow-save-append
   "M-v" meow-yank
   "M-V" meow-replace))

(defun c4/load-module (name)
  "Edit a configuration module"
  (interactive "sEdit which module?: ")
  (let* ((path (expand-file-name "modules" c4/config-dir))
          (module (expand-file-name (concat name ".el") path)))
    (find-file module)))

(defun c4/reload ()
  "Reload configuration"
  (interactive)
  (load-file user-init-file))

(defun c4/configure ()
  "Edit configuration"
  (interactive)
  (find-file user-init-file))

;; gxx:
;;
;; o i u  | 0 1 2
;; j k l  | 3 4 5
;; m , . | 6 7 8
;;
;; Mxx:
;;
;; q w e | 0 1 2
;; a s d | 3 4 5
;; z x c | 6 7 8
;;
;; mxx:
;;
;; r t y | 0 1 2
;; f g h | 3 4 5
;; v b n | 6 7 8

(setup commands:global
  (global-unset-key (kbd "M-SPC"))
  (:global
    "M-SPC u u" c4/configure ; g00
    "M-SPC u i" c4/load-module ; g01
    "M-SPC u o" c4/reload ; g02

    "M-SPC i u" eval-last-sexp ; g10
    "M-SPC i i" eval-defun ; g11
    "M-SPC i o" eval-region ; g12
    "M-SPC i j" eval-buffer ; g13

    "M-SPC o u" switch-to-buffer ; g20
    "M-SPC o i" switch-to-buffer-other-window ; g21
    "M-SPC o o" kill-this-buffer ; g22
    "M-SPC o j" kill-some-buffers ; g23
    "M-SPC o k" save-buffer ; g24
    "M-SPC o l" save-some-buffers ; g25

    "M-SPC j u" widen ; g30
    "M-SPC j i" narrow-to-defun ; g31
    "M-SPC j o" narow-to-page ; g32
    "M-SPC j j" narrow-to-region ; g33

    "M-SPC k u" find-file ; g40
    "M-SPC k i" find-file-other-window ; g41
    "M-SPC k o" dired ; g42
    "M-SPC k j" dired-other-window ; g43

    "M-SPC l u" save-buffers-kill-emacs ; g50
    "M-SPC l i" kill-emacs ; g51
    "M-SPC l o" restart-emacs ; g52
    "M-SPC l j" server-start ; g53
    "M-SPC l k" server-force-delete ; g54
    "M-SPC l l" recentf-open-files ; g55
    "M-SPC l m" delete-frame ; g56

    "M-SPC m u" project-switch-project ; g60
    "M-SPC m i" project-switch-to-buffer ; g61
    "M-SPC m o" project-kill-buffers ; g62
    "M-SPC m j" project-eshell ; g63
    "M-SPC m k" project-shell ; g64
    "M-SPC m l" project-shell-command ; g65
    "M-SPC m m" project-find-file ; g66

   ;;; g7x prefix reserved for +project module bindings

    "M-SPC . u" other-window ; g80
    "M-SPC . i" delete-window ; g81
    "M-SPC . o" delete-other-windows ; g82
    "M-SPC . j" split-window-below ; g83
    "M-SPC . k" split-window-right) ; g84

  (recentf-mode 1)
  (run-at-time nil (* 10 60) 'recentf-save-list)
  (:option
    recentf-max-menu-items 7
    recentf-max-saved-items 14))

(setup commands:minor
  (:global
   "M-SPC r r" info-emacs-manual ; m00
   "M-SPC r t" view-emacs-FAQ)) ; m01

(provide '+keybindings)

;;; +keybindings.el ends here
