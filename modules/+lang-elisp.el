;;; +lang-elisp.el --- -*- lexical-binding: t; -*-
;;;
;;; Summary: This module contains packages and configuration for working with
;;; and evaluating Elisp.

(setup (:straight eros)
  (:require eros)
  (:with-mode emacs-lisp-mode
    (:hook eros-mode)
    (:bind
      "M-SPC i u" eros-eval-last-sexp ; g10
      "M-SPC i i" eros-eval-defun))) ; g11

(provide '+lang-elisp)
;;; +lang-elisp.el ends here
